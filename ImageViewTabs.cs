﻿using System;

using Xamarin.Forms;

namespace XUpload
{
    public class ImageViewTabs : ContentPage
    {
        public ImageViewTabs()
        {
            var pickPhoto = new ContentPage{

                Title = "Pick Photo"
            };
			var pickVideo = new ContentPage
			{
                Title = "Pick Video"
			};
            var mainPage = new TabbedPage
            {
                Children = {
                    pickPhoto,
                    pickVideo
                }
            };

        }
    }
}

