﻿using Xamarin.Forms;
using Plugin.Media;
using Plugin.Media.Abstractions;

namespace XUpload
{
    public partial class XUploadPage : ContentPage
    {
        public XUploadPage()
        {
            InitializeComponent();
            //calling the internal method
            Media();

        }

        void Media()
        {
            //Event handler for uploading an image 
            pickPhoto.Clicked += async (sender, e) => 
            {
                if (!CrossMedia.Current.IsPickVideoSupported)
                { 
                    await DisplayAlert("Photo not Supported", "Permission not Granted","Ok");
                    return; }
                var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions()
                {
                    PhotoSize = PhotoSize.Medium
                });
                if (file == null)
                    return;

                image.Source = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    file.GetStream();
                    return stream;
                });
            };

            //Event handler for uploading a video
            uploadVid.Clicked += async (sender, e) => 
            {
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert("Video not Supported", "Permissin not Granted", "Ok");
                    return; }
                var file = await CrossMedia.Current.PickVideoAsync();

                if (file == null)
                    return;

                await DisplayAlert("Video Selected", "Location" + file.Path, "OK");
                file.Dispose();
            };

            //Event handler for taking photo
            takePhoto.Clicked += async (sender, e) => 
            {
                if(!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                {
                    await DisplayAlert("No Camera", "No Camera Available", "Ok");
                    return;
                }

                var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                {
                    PhotoSize = PhotoSize.Medium,
                    Name = "video.mp4",
                    Directory = "DefaultVideos"

                });

                if (file == null)
                    return;
                await DisplayAlert("File Location", file.Path,"Ok");

                image.Source = ImageSource.FromStream(()=>
                {
                    var stream = file.GetStream();
                    file.Dispose();
                    return stream;
                });
            };

            //Event handler for taking videos
            takeVid.Clicked += async (sender, e) => 
            {
                if(!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakeVideoSupported)
                {
                    await DisplayAlert("No Camera", "No Camera Available", "Ok");
                    return;
                }

                var file = await CrossMedia.Current.TakeVideoAsync(new StoreVideoOptions()
                {
                    Name = "jpg",
                    Directory = "Sample",
					PhotoSize = PhotoSize.Medium,
                });

                await DisplayAlert("Video Recorded", "Location" + file.Path, "Ok");

                file.Dispose();
            };

        }
    }
}
