﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace XUpload
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //MainPage = new XUploadPage();

            //MainPage = new ImageViewTabs();
            MainPage = new FinPage();
           // GoToMainPage();
        }

		public static void GoToMainPage()
		{
            var pickPhoto = new Button 
            {
                Text = "Pick Photo"
            };

			var pickVid = new Button
			{
				Text = "Pick Video"
			};

            var takePhoto = new Button
            {
                Text = "Take Photo"
            };

            var takeVideo = new Button 
            {
                Text = "Take Video"
            };

            var tab1 = new ContentPage
            {
                Title="Browse Photo",
                Content = new StackLayout{
                    Padding = 20,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalOptions = LayoutOptions.Center,
                    Children = {
                        pickPhoto,
                        pickVid
                    }
                }
            };

            var tab2 = new ContentPage 
            {
                Title="Browse Video",
                Content =  new StackLayout{
                    Padding = 20,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalOptions = LayoutOptions.Center,
                    Children = {
                        takePhoto,
                        takeVideo
                    }
                }
            };

			Current.MainPage = new TabbedPage
			{
				Children = {
                    tab1,
                    tab2
				}
			};
		}

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
