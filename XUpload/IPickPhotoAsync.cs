﻿using System;
using System.Threading.Tasks;
using Plugin.Media.Abstractions;

namespace XUpload
{
    public interface IPickPhotoAsync
    {
        Task<string> PickPhotoAsync();
    }
}
